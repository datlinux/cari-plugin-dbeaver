<!-- This file is used for a plugin's help display, code blocks must be indented. -->

# About

[DBeaver](https://dbeaver.io) plugin for the [Carinthia (cari) version manager](https://gitlab.com/datlinux/cari.git).

> See [this guide](https://gitlab.com/datlinux/cari/-/blob/main/README.md) for installing cari.

**Linux 64bit-AMD/Intel only...**

# Dependencies

- Java JDK 11+, Carinthia (`cari`), `bash`, `curl`, `tar`, `markdown`, generic POSIX utilities.

# Get the plugin:

	cari add dbeaver https://gitlab.com/datlinux/cari-plugin-dbeaver.git
	# Or..
	cari add dbeaver # No need to specify path as it's a default plugin..

# Install package:

	# Show all installable versions
	cari list dbeaver
	
	# Install latest version
	cari install dbeaver latest
	
	# Install specific version
	cari install dbeaver v.x
	
	# Set latest version globally
	cari global dbeaver latest
	
	# Set specific version globally
	cari global dbeaver v.x
	
	# Use use 'cari list',  'cari list dbeaver' and 'cari latest dbeaver' when checking for updates
	
	# Run
	dbeaver

# Uninstall package:

	cari uninstall dbeaver <version>

# Remove plugin (and all installed packages/versions!):

	cari remove dbeaver
